import turtle


def draw_border(side, position):
    # Draw border function
    border_pen = turtle.Turtle()
    border_pen.speed(0)
    border_pen.color('white')
    border_pen.penup()
    border_pen.setposition(-position, -position)
    border_pen.pensize(3)
    border_pen.hideturtle()
    border_pen.pendown()
    for i in range(4):
        border_pen.fd(side)
        border_pen.lt(90)
