# Turtle Invaders
# Python 3.6 Linux Mint 18.0

# Imports
import turtle
import os
from draw import draw_border
import math
import random

############################################
### Set up the screen | mw = main window ###
############################################

mw = turtle.Screen()
mw.bgcolor('black')
mw.title('Turtle Invaders')
mw.bgpic('space_invaders_background.gif')

# Registr shapes
turtle.register_shape('invader.gif')
turtle.register_shape('player.gif')

#####################
#### Draw border ####
#####################

# Call the function from draw.py (just for fun of it :D)
draw_border(600, 300)

####################
###### Score #######
####################
score = 0
score_pen = turtle.Turtle()
score_pen.speed(0)
score_pen.color('white')
score_pen.penup()
score_pen.setposition(-290, 270)
scorestring = 'Score: {}'.format(score)
score_pen.write(scorestring, False, align='left', font=('Arial', 14, 'normal'))
score_pen.hideturtle()


####################
###### Player ######
####################
player = turtle.Turtle()
player.color('green')
player.shape('player.gif')
player.penup()
player.speed(0)
player.setposition(0, -250)
player.setheading(90)

playerspeed = 15

####################
###### Enemy #######
####################

# More enemies
number_enemies = 5
enemies = []

# Add enemies to the list
for i in range(number_enemies):
    enemies.append(turtle.Turtle())

for enemy in enemies:
    enemy.penup()
    enemy.color('red')
    enemy.shape('invader.gif')
    enemy.speed(0)
    x = random.randint(-200, 200)
    y = random.randint(100, 250)
    enemy.setposition(x, y)

enemyspeed = 4

#####################
###### Bullet #######
#####################

bullet = turtle.Turtle()
bullet.color('yellow')
bullet.shape('triangle')
bullet.penup()
bullet.speed(0)
bullet.setheading(90)
bullet.shapesize(0.5, 0.5)
bullet.hideturtle()

bulletspeed = 20

# Bullet state
# ready - ready to fire
#fire - bullet is firing
bulletstate = 'ready'


#################
### Functions ###
#################


def move_left():
    x = player.xcor()
    x -= playerspeed
    # Left boundary - stay in frame
    if x < -280:
        x = -280
    player.setx(x)


def move_right():
    x = player.xcor()
    x += playerspeed
    # Right boundary - stay in frame
    if x > 280:
        x = 280
    player.setx(x)


def fire_bullet():
    # Test of declaring the global variable
    global bulletstate
    if bulletstate == 'ready':
        # Move the bullet above the player
        x = player.xcor()
        y = player.ycor() + 10
        bullet.setposition(x, y)
        bullet.showturtle()
        # Change the bulletstate
        bulletstate = 'fire'


def is_collision(t1, t2):
    distance = math.sqrt(math.pow(t1.xcor() - t2.xcor(), 2) +
                         math.pow(t1.ycor() - t2.ycor(), 2))
    if distance < 15:
        return True
    else:
        return False


#########################
### Keyboard bindings ###
#########################


turtle.listen()
turtle.onkey(move_left, "Left")
turtle.onkey(move_right, "Right")
turtle.onkey(fire_bullet, "space")


##########################
##### MAIN GAME LOOP #####
##########################

while True:

    for enemy in enemies:
        # Move the enemy
        x = enemy.xcor()
        x += enemyspeed
        enemy.setx(x)

        # Move the enemy back and down on boundary
        if enemy.xcor() > 280:
            for e in enemies:
                y = e.ycor()
                y -= 40
                e.sety(y)
            enemyspeed *= -1
        if enemy.xcor() < -280:
            for e in enemies:
                y = e.ycor()
                y -= 40
                e.sety(y)
            enemyspeed *= -1

        # Check for collisions
        if is_collision(bullet, enemy):
            # Reset the bullet
            bullet.hideturtle()
            bulletstate = 'ready'
            bullet.setposition(0, -400)

            # Reset the enemy
            x = random.randint(-200, 200)
            y = random.randint(-100, 250)
            enemy.setposition(x, y)

            # Update score
            score += 10
            scorestring = 'Score: {}'.format(score)
            score_pen.clear()
            score_pen.write(scorestring, False,
                            align='left', font=('Arial', 14, 'normal'))

        if is_collision(player, enemy) or enemy.ycor() < -280:
            player.hideturtle()
            for enemy in enemies:
                enemy.hideturtle()
            gameover_pen = turtle.Turtle()
            gameover_pen.speed(0)
            gameover_pen.color('white')
            gameover_pen.penup()
            gameoverstring = 'GAME OVER'
            gameover_pen.write(gameoverstring, False,
                               align='center', font=('Arial', 48, 'bold'))
            gameover_pen.hideturtle()
            # break

    # Move the bullet
    y = bullet.ycor()
    y += bulletspeed
    bullet.sety(y)

    # Bullet border boundary
    if bullet.ycor() > 265:
        bullet.hideturtle()
        bulletstate = 'ready'
